ChangeLog
=========

Unreleased
----------

2.3.0 (2025-02-06)
------------------

* Add support for python 3.13.
* Drop support for python 3.8.
* Expose ``LogEntry`` (#32).
* Update project setup.

2.2.0 (2024-09-11)
------------------

* Upgrade to fred-frgal ~= 3.15 (#31)

2.1.1 (2024-02-05)
------------------

* Ignore registration of empty log entry types and object references (#30).

2.1.0 (2024-01-18)
------------------

* Add log entry list filtering by properties (#29).
* Use ruff for linting and autoformatting.

2.0.2 (2023-09-12)
------------------

* Fix dependency specification.

2.0.1 (2022-11-08)
------------------

* Fix annotations.

2.0.0 (2022-04-19)
------------------

* Rename grill to fred-grill.
* Drop deprecated properties field from log entry info.
* Drop deprecated function ``get_logger``.
* Drop deprecated ``grill.grpc`` module.
* Update decoding of unset messages and empty strings.

1.7.4 (2022-04-14)
------------------

* Remove deprecated automagical awaitables in tests.

1.7.3 (2022-04-12)
------------------

* Fix typing of TestLogEntry.

1.7.2 (2022-04-06)
------------------

* Use MutableMapping instead of Mapping in Properties and References types.

1.7.1 (2022-04-04)
------------------

* Update Properties and References types.
* Fix mypy checks.

1.7.0 (2022-02-09)
------------------

* Add support for python 3.10.
* Drop support for python 3.6.
* Support fred-api-logger 4.1 (#20).
* Add async SearchLoggerHistory client (#19).
* Decode exceptions from SearchLoggerHistory (#18).
* Update static checks and fix annotations.

1.6.0 (2021-11-16)
------------------

* Add ``TestLogEntry`` object (#13).
* Allow simple result registration (#16).
* Fix results on empty responses.
* Fix annotations on ``wrap_rpc_errors`` decorator.
* Update static checks.

1.5.1 (2021-10-20)
------------------

* Rename utility ``get_logger`` to ``get_logger_client`` and expose (#12).
* Fix annotations (#14).

1.5.0 (2021-10-18)
------------------

* Rename module ``grpc`` to ``clients`` (#6).
* Add abstract logger class and dummy and test loggers (#5, #7, #8).
* Add utility ``get_logger`` to facilite creation of logger instance (#10).
* Allow simple properties (``dict``) in arguments (#2).
* Add python 3.9.
* Drop python 3.5.
* Use frgal >= 3.7.
* Use ``setup.cfg`` for project setup.
* Update static checks and CI.

1.4.0 (2021-01-25)
------------------

* Translate some of gRPC exceptions to specific exceptions.
* Add ``register_*`` methods to ``Logger`` API.
* Drop support for ``Logger`` API < 3.2.

1.3.2 (2020-11-12)
------------------

* Fix version specifier for frgal.

1.3.1 (2020-11-05)
------------------

* Support multiple versions of logger API.
* Rename CHANGELOG and fix its format.

1.3.0 (2019-10-21)
------------------

* Add python 3.8.
* Add missing methods to ``LoggerClient``.
* Add ``SearchLoggerHistoryClient``.
* Rewrite tests to test client.
* Update static checks and CI.
* Use pypi.nic.cz to provide dependencies.
* Drop ``constraints.txt``.
* Reformat ChangeLog.
* Add license.

1.2.0 (2019-09-17)
------------------

* Update version of Logger API (includes diagnostic methods)

1.1.0 (2019-07-03)
------------------

* Update version of Logger API.

1.0.0 (2019-06-03)
------------------

* Initial version
