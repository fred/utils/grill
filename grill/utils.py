"""Various utilities."""

import inspect
from dataclasses import dataclass, field
from typing import Any, Optional, cast
from unittest.mock import Mock, _Call, call, sentinel

from .clients import AbstractLoggerClient


@dataclass
class TestLogEntry:
    """Represents a single log entry in tests."""

    service: str
    log_entry_type: str
    result: str
    source_ip: Optional[str] = None
    input_session_id: Optional[str] = None
    input_content: Optional[str] = None
    # Annotating the properties and references as Properties and References complicates the usage.
    # Despite the Dict[str, Any] is too lenient, it makes the TestLogEntry usable.
    input_properties: Optional[dict[str, Any]] = None
    input_references: Optional[dict[str, Any]] = None
    session_id: Optional[str] = None
    content: Optional[str] = None
    properties: dict[str, Any] = field(default_factory=dict)
    references: dict[str, Any] = field(default_factory=dict)

    def get_calls(self) -> list[_Call]:
        """Return a list of calls to create this log entry."""
        create_call = call.create_log_entry(
            self.service,
            self.log_entry_type,
            source_ip=self.source_ip,
            session_id=self.input_session_id,
            content=self.input_content,
            properties=self.input_properties,
            references=self.input_references,
        )
        close_call = call.close_log_entry(
            sentinel.log_entry_id,
            self.result,
            session_id=self.session_id,
            content=self.content,
            properties=self.properties,
            references=self.references,
        )
        return [create_call, close_call]


class TestLoggerClient(AbstractLoggerClient):
    """Logger client which helps to test the logging."""

    def __init__(self) -> None:
        self.mock = Mock()

    def _call(self, *args: Any, **kwargs: Any) -> None:
        """Check arguments and mark the call."""
        # Get the name of method which called this method.
        function = inspect.stack()[1].function
        # Check arguments matches the method signature.
        sig = inspect.signature(getattr(AbstractLoggerClient, function))
        sig.bind(self, *args, **kwargs)
        # Mark the call into the mock.
        getattr(self.mock, function)(*args, **kwargs)

    def create_log_entry(self, *args: Any, **kwargs: Any) -> str:  # noqa: D417
        """Create log entry.

        Args:
            service: Name of the logger service.
            log_entry_type: Name of the log entry type.
            source_ip: IP address of the action source.
            session_id: Identifier of the log session.
            content: Input content.
            properties: Input properties.
            references: Input references.
        """
        self._call(*args, **kwargs)
        return cast(str, sentinel.log_entry_id)

    def close_log_entry(self, *args: Any, **kwargs: Any) -> None:  # noqa: D417
        """Close log entry.

        Args:
            log_entry_id: Identifier of the log entry to close.
            result: Name of the result.
            session_id: Identifier of the log session.
            content: Output content.
            properties: Output properties.
            references: Output references.
        """
        self._call(*args, **kwargs)

    def create_session(self, *args: Any, **kwargs: Any) -> str:  # noqa: D417
        """Create session.

        Args:
            user_id: User's identifier.
            username: User's username.
        """
        self._call(*args, **kwargs)
        return cast(str, sentinel.session_id)

    def close_session(self, *args: Any, **kwargs: Any) -> None:  # noqa: D417
        """Close session.

        Args:
            session_id: Identifier of the session to close.
        """
        self._call(*args, **kwargs)

    def get_log_entry_types(self, *args: Any, **kwargs: Any) -> list[str]:  # noqa: D417
        """Send gRPC request to load log entry types.

        Args:
            service: Identifier of the requested service.
        """
        self._call(*args, **kwargs)
        return []

    def get_services(self, *args: Any, **kwargs: Any) -> list[str]:  # noqa: D417
        """Send gRPC request to load all defined services.

        Args:
            No arguments
        """
        self._call(*args, **kwargs)
        return []

    def get_results(self, *args: Any, **kwargs: Any) -> dict[str, int]:  # noqa: D417
        """Send gRPC request to load all defined results.

        Args:
            service: Identifier of the requested service.
        """
        self._call(*args, **kwargs)
        return {}

    def get_object_reference_types(self, *args: Any, **kwargs: Any) -> list[str]:  # noqa: D417
        """Send gRPC request to get defined types of the object reference.

        Args:
            No arguments
        """
        self._call(*args, **kwargs)
        return []

    def register_service(self, *args: Any, **kwargs: Any) -> None:  # noqa: D417
        """Send gRPC request to register a new service.

        Args:
            service: Identifier of the requested service.
            handle: Internal identifier of the requested service.
        """
        self._call(*args, **kwargs)

    def register_results(self, *args: Any, **kwargs: Any) -> None:  # noqa: D417
        """Send gRPC request to register a new service results.

        Args:
            service: Identifier of the requested service.
            results: Mapping of result names to codes.
        """
        self._call(*args, **kwargs)

    def register_log_entry_types(self, *args: Any, **kwargs: Any) -> None:  # noqa: D417
        """Send gRPC request to register a new service log entry types.

        Args:
            service: Identifier of the requested service.
            log_entry_types: Iterable with the log entry types.
        """
        self._call(*args, **kwargs)

    def register_object_reference_types(self, *args: Any, **kwargs: Any) -> None:  # noqa: D417
        """Send gRPC request to register a new reference types.

        Args:
            reference_types: Iterable with the reference types.
        """
        self._call(*args, **kwargs)
