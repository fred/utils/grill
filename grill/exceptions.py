"""Grill exceptions."""

from frgal.exceptions import FrgalException


class LoggerException(Exception):
    """Base exception for logger interfaces."""


class LogEntryDoesNotExist(FrgalException, LoggerException):
    """Log entry was not found."""


class SessionDoesNotExist(FrgalException, LoggerException):
    """Session was not found."""


class InvalidArgument(ValueError, LoggerException):
    """Server returned INVALID_ARGUMENT status."""
