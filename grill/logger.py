"""Logger objects and its components."""

from types import TracebackType
from typing import Optional

from typing_extensions import Literal

from .clients import AbstractLoggerClient, Properties, References


class Logger:
    """Logger - manages logging."""

    def __init__(self, client: AbstractLoggerClient, service: str, default_result: str):
        """Initialize a logger.

        Args:
            client: A logger client instance.
            service: Name of the logger service.
            default_result: Name of the default result.
        """
        self.client = client
        self.service = service
        self.default_result = default_result

    def create(
        self,
        log_entry_type: str,
        *,
        source_ip: Optional[str] = None,
        session_id: Optional[str] = None,
        content: Optional[str] = None,
        properties: Optional[Properties] = None,
        references: Optional[References] = None,
    ) -> "LogEntry":
        """Create new log entry.

        Args:
            log_entry_type: Name of the log entry type.
            source_ip: IP address of the action source.
            session_id: Identifier of the log session.
            content: Input content.
            properties: Input properties.
            references: Input references.
        """
        return LogEntry(
            self,
            log_entry_type,
            source_ip=source_ip,
            session_id=session_id,
            content=content,
            properties=properties,
            references=references,
        )

    def create_session(self, user_id: int, username: str) -> str:
        """Create new session.

        Args:
            user_id: User's identifier.
            username: User's username.
        """
        return self.client.create_session(user_id, username)

    def close_session(self, session_id: str) -> None:
        """Create new session.

        Args:
            session_id: Identifier of the session to close.
        """
        self.client.close_session(session_id)


class LogEntry:
    """Represents single log entry.

    Can be used as a context manager.

    Attributes:
        logger: A logger instance.
        log_entry_type: Name of the log entry type.
        source_ip: IP address of the action source.
        session_id: Identifier of the log session.
        input_content: Input content.
        input_properties: Input properties.
        input_references: Input references.
        content: Output content.
        properties: Output properties.
        references: Output references.
        result: Name of the result.
        entry_id: Identifier of the log entry.
    """

    def __init__(
        self,
        logger: Logger,
        log_entry_type: str,
        *,
        source_ip: Optional[str] = None,
        session_id: Optional[str] = None,
        content: Optional[str] = None,
        properties: Optional[Properties] = None,
        references: Optional[References] = None,
    ):
        """Initialize a log entry.

        Args:
            logger: A logger instance.
            log_entry_type: Name of the log entry type.
            source_ip: IP address of the action source.
            session_id: Identifier of the log session.
            content: Input content.
            properties: Input properties.
            references: Input references.
        """
        self.logger = logger
        self.log_entry_type = log_entry_type
        self.source_ip = source_ip
        self.session_id = session_id
        self.input_content = content
        self.input_properties = properties
        self.input_references = references
        self.content = None  # type: Optional[str]
        self.properties = {}  # type: Properties
        self.references = {}  # type: References
        self.result = None  # type: Optional[str]
        self.entry_id = None  # type: Optional[str]

    def open(self) -> None:
        """Open the log entry."""
        self.entry_id = self.logger.client.create_log_entry(
            self.logger.service,
            self.log_entry_type,
            source_ip=self.source_ip,
            session_id=self.session_id,
            content=self.input_content,
            properties=self.input_properties,
            references=self.input_references,
        )

    def close(self) -> None:
        """Close the log entry."""
        assert self.entry_id is not None  # noqa: S101
        self.logger.client.close_log_entry(
            self.entry_id,
            self.result or self.logger.default_result,
            session_id=self.session_id,
            content=self.content,
            properties=self.properties,
            references=self.references,
        )

    def __enter__(self) -> "LogEntry":
        """Enter the context manager and open the log entry."""
        self.open()
        return self

    def __exit__(
        self,
        exc_type: Optional[type[BaseException]],
        exc_value: Optional[BaseException],
        traceback: Optional[TracebackType],
    ) -> Literal[False]:
        """Exit the context manager and close the log entry."""
        self.close()
        # Do not suppress the exceptions from within the context manager.
        return False
