"""Logger gRPC clients."""

from abc import ABC, abstractmethod
from collections.abc import Iterable, Mapping, MutableMapping
from datetime import datetime
from functools import wraps
from importlib import import_module
from typing import (
    Any,
    Callable,
    NamedTuple,
    Optional,
    TypeVar,
    Union,
    cast,
)

from fred_api.logger.log_entry_common_types_pb2 import (
    LogEntryDoesNotExist as GrpcLogEntryDoesNotExist,
    LogEntryProperty,
)
from fred_api.logger.service_logger_grpc_pb2 import (
    CloseLogEntryRequest,
    CloseSessionReply,
    CloseSessionRequest,
    CreateLogEntryRequest,
    CreateSessionRequest,
    GetLogEntryTypesRequest,
    GetResultsRequest,
    RegisterLogEntryTypesRequest,
    RegisterObjectReferenceTypesRequest,
    RegisterResultsRequest,
    RegisterServiceRequest,
)
from fred_api.logger.service_logger_grpc_pb2_grpc import LoggerStub
from fred_api.logger.service_search_logger_history_grpc_pb2 import (
    GetLogEntryInfoRequest,
    ListLogEntriesReply,
    ListLogEntriesRequest,
)
from fred_api.logger.service_search_logger_history_grpc_pb2_grpc import SearchLoggerHistoryStub
from frgal import GrpcClient, GrpcDecoder
from frgal.aio import AsyncGrpcClient, SyncGrpcProxy
from google.protobuf.empty_pb2 import Empty as _google_protobuf_Empty
from grpc import RpcError, StatusCode

from .exceptions import InvalidArgument, LogEntryDoesNotExist, SessionDoesNotExist


# Structure for a log property
class LogProperty(NamedTuple):
    """Log property."""

    value: str
    children: Optional[MutableMapping[str, str]]


# Custom types
Properties = MutableMapping[str, Union[LogProperty, Iterable[LogProperty], str, Iterable[str]]]
References = MutableMapping[str, Union[str, Iterable[str]]]
User = Union[str, int]


# Property used by list_log_entries
class ListProperty(NamedTuple):
    """List property."""

    name: str
    value: str


def get_logger_client(logger: str, **kwargs: Any) -> "AbstractLoggerClient":
    """Return a logger instance from dotted path and arguments.

    Args:
        logger: A dotted path to the logger class.
        **kwargs: Arguments to initialize the logger.

    Raises:
        ImportError: If the dotted path is not valid.
    """
    try:
        module_name, class_name = logger.rsplit(".", 1)
    except ValueError as error:
        raise ImportError("'{}' is not a valid dotted path.".format(logger)) from error

    try:
        module = import_module(module_name)
    except ValueError as error:
        raise ImportError(str(error)) from error

    try:
        logger_cls = getattr(module, class_name)
    except AttributeError as error:
        raise ImportError("'{}' does not have a class or attribute '{}'.".format(module_name, class_name)) from error

    return cast(AbstractLoggerClient, logger_cls(**kwargs))


def _to_tuple(value: Any) -> Iterable:
    """Transform value into a tuple."""
    if isinstance(value, Iterable) and not isinstance(value, (str, LogProperty)):
        return tuple(value)
    return (value,)


def _to_property(value: Union[str, LogProperty]) -> LogProperty:
    """Transform value into a LogProperty."""
    if isinstance(value, str):
        return LogProperty(value=value, children=None)
    return value


F = TypeVar("F", bound=Callable[..., Any])


def wrap_rpc_errors(func: F) -> F:
    """Decorate function and wrap gRPC errors."""

    @wraps(func)
    def wrapper(*args: Any, **kwargs: Any) -> Any:
        try:
            return func(*args, **kwargs)
        except RpcError as error:
            if error.code() == StatusCode.INVALID_ARGUMENT:
                raise InvalidArgument(error.details()) from error
            raise

    # XXX: Mypy doesn't consider '...' from 'F' type var and '*args, **kwargs' from wrapper to be equal.
    # This could be fixed in future using 'ParamSpec'.
    return cast(F, wrapper)


class AbstractLoggerClient(ABC):
    """Abstract logger client."""

    @abstractmethod
    def create_log_entry(
        self,
        service: str,
        log_entry_type: str,
        *,
        source_ip: Optional[str] = None,
        session_id: Optional[str] = None,
        content: Optional[str] = None,
        properties: Optional[Properties] = None,
        references: Optional[References] = None,
    ) -> str:
        """Create log entry.

        Args:
            service: Name of the logger service.
            log_entry_type: Name of the log entry type.
            source_ip: IP address of the action source.
            session_id: Identifier of the log session.
            content: Input content.
            properties: Input properties.
            references: Input references.
        """

    @abstractmethod
    def close_log_entry(
        self,
        log_entry_id: str,
        result: str,
        *,
        session_id: Optional[str] = None,
        content: Optional[str] = None,
        properties: Optional[Properties] = None,
        references: Optional[References] = None,
    ) -> None:
        """Close log entry.

        Args:
            log_entry_id: Identifier of the log entry to close.
            result: Name of the result.
            session_id: Identifier of the log session.
            content: Output content.
            properties: Output properties.
            references: Output references.
        """

    @abstractmethod
    def create_session(self, user_id: int, username: str) -> str:
        """Create session.

        Args:
            user_id: User's identifier.
            username: User's username.
        """

    @abstractmethod
    def close_session(self, session_id: str) -> None:
        """Close session.

        Args:
            session_id: Identifier of the session to close.
        """

    @abstractmethod
    def get_log_entry_types(self, service: str) -> list[str]:
        """Send gRPC request to load log entry types.

        Args:
            service: Identifier of the requested service.
        """

    @abstractmethod
    def get_services(self) -> list[str]:
        """Send gRPC request to load all defined services.

        Args:
            No arguments
        """

    @abstractmethod
    def get_results(self, service: str) -> dict[str, int]:
        """Send gRPC request to load all defined results.

        Args:
            service: Identifier of the requested service.
        """

    @abstractmethod
    def get_object_reference_types(self) -> list[str]:
        """Send gRPC request to get defined types of the object reference.

        Args:
            No arguments
        """

    @abstractmethod
    def register_service(self, service: str, handle: str) -> None:
        """Send gRPC request to register a new service.

        Args:
            service: Identifier of the requested service.
            handle: Internal identifier of the requested service.
        """

    @abstractmethod
    def register_results(self, service: str, results: Union[Iterable[str], dict[str, int]]) -> None:
        """Send gRPC request to register a new service results.

        Args:
            service: Identifier of the requested service.
            results: Mapping of result names to codes.
        """

    @abstractmethod
    def register_log_entry_types(self, service: str, log_entry_types: Iterable[str]) -> None:
        """Send gRPC request to register a new service log entry types.

        Args:
            service: Identifier of the requested service.
            log_entry_types: Iterable with the log entry types.
        """

    @abstractmethod
    def register_object_reference_types(self, reference_types: Iterable[str]) -> None:
        """Send gRPC request to register a new reference types.

        Args:
            reference_types: Iterable with the reference types.
        """


class DummyLoggerClient(AbstractLoggerClient):
    """Logger client which actually doesn't do anything."""

    def create_log_entry(
        self,
        service: str,
        log_entry_type: str,
        *,
        source_ip: Optional[str] = None,
        session_id: Optional[str] = None,
        content: Optional[str] = None,
        properties: Optional[Properties] = None,
        references: Optional[References] = None,
    ) -> str:
        """Create log entry.

        Args:
            service: Name of the logger service.
            log_entry_type: Name of the log entry type.
            source_ip: IP address of the action source.
            session_id: Identifier of the log session.
            content: Input content.
            properties: Input properties.
            references: Input references.
        """
        return "DUMMY_LOG_ENTRY_ID"

    def close_log_entry(
        self,
        log_entry_id: str,
        result: str,
        *,
        session_id: Optional[str] = None,
        content: Optional[str] = None,
        properties: Optional[Properties] = None,
        references: Optional[References] = None,
    ) -> None:
        """Close log entry.

        Args:
            log_entry_id: Identifier of the log entry to close.
            result: Name of the result.
            session_id: Identifier of the log session.
            content: Output content.
            properties: Output properties.
            references: Output references.
        """

    def create_session(self, user_id: int, username: str) -> str:
        """Create session.

        Args:
            user_id: User's identifier.
            username: User's username.
        """
        return "DUMMY_SESSION_ID"

    def close_session(self, session_id: str) -> None:
        """Close session.

        Args:
            session_id: Identifier of the session to close.
        """

    def get_log_entry_types(self, service: str) -> list[str]:
        """Send gRPC request to load log entry types.

        Args:
            service: Identifier of the requested service.
        """
        return []

    def get_services(self) -> list[str]:
        """Send gRPC request to load all defined services.

        Args:
            No arguments
        """
        return []

    def get_results(self, service: str) -> dict[str, int]:
        """Send gRPC request to load all defined results.

        Args:
            service: Identifier of the requested service.
        """
        return {}

    def get_object_reference_types(self) -> list[str]:
        """Send gRPC request to get defined types of the object reference.

        Args:
            No arguments
        """
        return []

    def register_service(self, service: str, handle: str) -> None:
        """Send gRPC request to register a new service.

        Args:
            service: Identifier of the requested service.
            handle: Internal identifier of the requested service.
        """

    def register_results(self, service: str, results: Union[Iterable[str], dict[str, int]]) -> None:
        """Send gRPC request to register a new service results.

        Args:
            service: Identifier of the requested service.
            results: Mapping of result names to codes.
        """

    def register_log_entry_types(self, service: str, log_entry_types: Iterable[str]) -> None:
        """Send gRPC request to register a new service log entry types.

        Args:
            service: Identifier of the requested service.
            log_entry_types: Iterable with the log entry types.
        """

    def register_object_reference_types(self, reference_types: Iterable[str]) -> None:
        """Send gRPC request to register a new reference types.

        Args:
            reference_types: Iterable with the reference types.
        """


class LoggerDecoder(GrpcDecoder):
    """Logger gRPC decoder."""

    decode_unset_messages = False
    decode_empty_string_none = False

    def __init__(self) -> None:
        """Initialize the decoder object by registering custom decoders."""
        super().__init__()
        self.set_exception_decoder(GrpcLogEntryDoesNotExist, LogEntryDoesNotExist)
        self.set_exception_decoder(CloseSessionReply.Exception.SessionDoesNotExist, SessionDoesNotExist)


class LoggerClient(AbstractLoggerClient, GrpcClient):
    """Logger gRPC client."""

    stub_cls = LoggerStub
    decoder_cls = LoggerDecoder

    def _update_log_request(
        self,
        request: Union[CreateLogEntryRequest, CloseLogEntryRequest],
        session_id: Optional[str] = None,
        content: Optional[str] = None,
        properties: Optional[Properties] = None,
        references: Optional[References] = None,
    ) -> None:
        """Update shared fields in create and close log entry requests."""
        if session_id:
            request.session_id.value = session_id
        if content:
            request.content.value = content
        properties = properties or {}
        for key, values in properties.items():
            values = (_to_property(i) for i in _to_tuple(values))
            for value in values:
                if value.children:
                    request.properties[key].values.add(value=value.value, children=value.children)
                else:
                    request.properties[key].values.add(value=value.value)
        references = references or {}
        for key, ref_values in references.items():
            for value in _to_tuple(ref_values):
                request.references[key].references.add(value=value)

    @wrap_rpc_errors
    def create_log_entry(
        self,
        service: str,
        log_entry_type: str,
        *,
        source_ip: Optional[str] = None,
        session_id: Optional[str] = None,
        content: Optional[str] = None,
        properties: Optional[Properties] = None,
        references: Optional[References] = None,
    ) -> str:
        """Send gRPC request to create log entry.

        Args:
            service: Name of the logger service.
            log_entry_type: Name of the log entry type.
            source_ip: IP address of the action source.
            session_id: Identifier of the log session.
            content: Input content.
            properties: Input properties.
            references: Input references.
        """
        request = CreateLogEntryRequest()
        request.service.value = service
        request.log_entry_type.value = log_entry_type
        if source_ip:
            request.source_ip.value = source_ip
        self._update_log_request(request, session_id, content, properties, references)

        return cast(str, self.call("create_log_entry", request))

    @wrap_rpc_errors
    def close_log_entry(
        self,
        log_entry_id: str,
        result: str,
        *,
        session_id: Optional[str] = None,
        content: Optional[str] = None,
        properties: Optional[Properties] = None,
        references: Optional[References] = None,
    ) -> None:
        """Send gRPC request to close log entry.

        Args:
            log_entry_id: Identifier of the log entry to close.
            result: Name of the result.
            session_id: Identifier of the log session.
            content: Output content.
            properties: Output properties.
            references: Output references.
        """
        request = CloseLogEntryRequest()
        request.log_entry_id.value = log_entry_id
        request.result_name = result
        self._update_log_request(request, session_id, content, properties, references)

        return cast(None, self.call("close_log_entry", request))

    @wrap_rpc_errors
    def create_session(self, user_id: int, username: str) -> str:
        """Send gRPC request to create session.

        Args:
            user_id: User's identifier.
            username: User's username.
        """
        request = CreateSessionRequest()
        request.user_id.value = user_id
        request.username.value = username
        return cast(str, self.call("create_session", request))

    @wrap_rpc_errors
    def close_session(self, session_id: str) -> None:
        """Send gRPC request to create session.

        Args:
            session_id: Identifier of the session to close.
        """
        request = CloseSessionRequest()
        request.session_id.value = session_id
        return cast(None, self.call("close_session", request))

    @wrap_rpc_errors
    def get_log_entry_types(self, service: str) -> list[str]:
        """Send gRPC request to load log entry types.

        Args:
            service: Identifier of the requested service.
        """
        request = GetLogEntryTypesRequest()
        request.service.value = service
        return cast(Optional[list[str]], self.call("get_log_entry_types", request)) or []

    @wrap_rpc_errors
    def get_services(self) -> list[str]:
        """Send gRPC request to load all defined services.

        Args:
            No arguments
        """
        return cast(Optional[list[str]], self.call("get_services", _google_protobuf_Empty())) or []

    @wrap_rpc_errors
    def get_results(self, service: str) -> dict[str, int]:
        """Send gRPC request to load all defined results.

        Args:
            service: Identifier of the requested service.
        """
        request = GetResultsRequest()
        request.service.value = service
        return cast(Optional[dict[str, int]], self.call("get_results", request)) or {}

    @wrap_rpc_errors
    def get_object_reference_types(self) -> list[str]:
        """Send gRPC request to get defined types of the object reference.

        Args:
            No arguments
        """
        return (
            cast(
                Optional[list[str]],
                self.call("get_object_reference_types", _google_protobuf_Empty()),
            )
            or []
        )

    @wrap_rpc_errors
    def register_service(self, service: str, handle: str) -> None:
        """Send gRPC request to register a new service.

        Args:
            service: Identifier of the requested service.
            handle: Internal identifier of the requested service.
        """
        request = RegisterServiceRequest()
        request.service_name.value = service
        request.service_handle = handle
        return cast(None, self.call("register_service", request))

    @wrap_rpc_errors
    def register_results(self, service: str, results: Union[Iterable[str], dict[str, int]]) -> None:
        """Send gRPC request to register a new service results.

        Args:
            service: Identifier of the requested service.
            results: Mapping of result names to codes.
        """
        request = RegisterResultsRequest()
        request.service.value = service
        if isinstance(results, Mapping):
            for name, code in results.items():
                request.results.add(name=name, code=code)
        else:
            known_results = self.get_results(service)
            if known_results:
                next_code = max(known_results.values()) + 1
            else:
                next_code = 0
            for name in results:
                if name in known_results:
                    continue
                request.results.add(name=name, code=next_code)
                next_code += 1

        if request.results:
            return cast(None, self.call("register_results", request))
        else:
            # There are no results to register.
            return None

    @wrap_rpc_errors
    def register_log_entry_types(self, service: str, log_entry_types: Iterable[str]) -> None:
        """Send gRPC request to register a new service log entry types.

        Args:
            service: Identifier of the requested service.
            log_entry_types: Iterable with the log entry types.
        """
        request = RegisterLogEntryTypesRequest()
        request.service.value = service
        for name in log_entry_types:
            request.log_entry_types.add(value=name)
        if request.log_entry_types:
            return cast(None, self.call("register_log_entry_types", request))
        else:
            # There are no log entry types to register
            return None

    @wrap_rpc_errors
    def register_object_reference_types(self, reference_types: Iterable[str]) -> None:
        """Send gRPC request to register a new reference types.

        Args:
            reference_types: Iterable with the reference types.
        """
        request = RegisterObjectReferenceTypesRequest()
        for name in reference_types:
            request.object_types.add(value=name)
        if request.object_types:
            return cast(None, self.call("register_object_reference_types", request))
        else:
            # There are no object references to register
            return None


class SearchLoggerHistoryDecoder(GrpcDecoder):
    """Logger gRPC decoder."""

    decode_unset_messages = False
    decode_empty_string_none = False

    def __init__(self) -> None:
        """Initialize the decoder object by registering custom decoders."""
        super().__init__()
        self.set_exception_decoder(GrpcLogEntryDoesNotExist, LogEntryDoesNotExist)
        self.set_exception_decoder(ListLogEntriesReply.Exception.LogEntryServiceNotFound, InvalidArgument)
        self.set_exception_decoder(ListLogEntriesReply.Exception.InvalidTimestampFrom, InvalidArgument)
        self.set_exception_decoder(ListLogEntriesReply.Exception.InvalidTimestampTo, InvalidArgument)
        self.set_exception_decoder(ListLogEntriesReply.Exception.InvalidTimestampRange, InvalidArgument)
        self.set_exception_decoder(ListLogEntriesReply.Exception.InvalidCountLimit, InvalidArgument)


class SearchLoggerHistoryAsyncClient(AsyncGrpcClient):
    """Base class for bot sync and async Logger History gRPC client."""

    stub_cls = SearchLoggerHistoryStub
    decoder_cls = SearchLoggerHistoryDecoder

    async def list_log_entries(
        self,
        service: str,
        timestamp_from: datetime,
        timestamp_to: datetime,
        limit: int,
        *,
        log_entry_types: Optional[list[str]] = None,
        user: Optional[User] = None,
        reference_type: Optional[str] = None,
        reference_value: Optional[str] = None,
        properties: Optional[Iterable[ListProperty]] = None,
    ) -> list[str]:
        """Send gRPC request to get log entry info.

        Args:
            service: Name of the logger service.
            timestamp_from: Begin of the time range filter applied to search.
            timestamp_to: End of the time range filter applied to search.
            limit: Number of the maximum count of results.
            log_entry_types: List of log entry type names.
            user: User name or user_id
            reference_type: Type of object referenced in searched log entries.
            reference_value: Object identifier referenced in searched log entries.
            properties: List of key, value pairs used for filtering by log properties.

        Returns:
            List of found log entry identifiers.
        """
        request = ListLogEntriesRequest()
        # required parameters
        request.service.value = service
        request.timestamp_from.FromDatetime(timestamp_from)
        request.timestamp_to.FromDatetime(timestamp_to)
        request.count_limit = limit

        for log_entry_type in log_entry_types or []:
            request.log_entry_types.add(value=log_entry_type)
        if user:
            if isinstance(user, int):
                request.user_id.value = user
            else:
                request.username.value = user
        if reference_type:
            request.reference.type.value = reference_type
        if reference_value:
            request.reference.value.value = reference_value

        if properties:
            request.properties.extend([LogEntryProperty(name=p.name, value=p.value) for p in properties])

        # The call may return `None`.
        return await self.call("list_log_entries", request) or []

    async def get_log_entry_info(self, log_entry_id: str) -> dict[str, Any]:
        """Send gRPC request to get log entry info.

        Args:
            log_entry_id: Identifier of the log entry to retrieve details about.

        Returns:
            dictionary: details of the requested log entry.
        """
        request = GetLogEntryInfoRequest()
        request.log_entry_id.value = log_entry_id
        log_entry_info = await self.call("get_log_entry_info", request)
        log_entry_info.pop("properties", None)  # Remove deprecated field `properties` from the result
        return cast(dict[str, Any], log_entry_info)


class SearchLoggerHistoryClient(SyncGrpcProxy):
    """Search Logger History gRPC client."""

    client_cls = SearchLoggerHistoryAsyncClient
