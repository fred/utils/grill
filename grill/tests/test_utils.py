from collections.abc import Iterable
from functools import partial
from unittest import TestCase
from unittest.mock import call, sentinel

from ..utils import TestLogEntry, TestLoggerClient


class TestLogEntryTest(TestCase):
    def test_get_calls_minimal(self):
        # Test get_calls with only required attributes.
        log_entry = TestLogEntry(sentinel.service, sentinel.log_entry_type, sentinel.result)
        create_call = call.create_log_entry(
            sentinel.service,
            sentinel.log_entry_type,
            source_ip=None,
            session_id=None,
            content=None,
            properties=None,
            references=None,
        )
        close_call = call.close_log_entry(
            sentinel.log_entry_id, sentinel.result, session_id=None, content=None, properties={}, references={}
        )
        self.assertEqual(log_entry.get_calls(), [create_call, close_call])

    def test_get_calls_all(self):
        # Test get_calls with all attributes.
        log_entry = TestLogEntry(
            sentinel.service,
            sentinel.log_entry_type,
            sentinel.result,
            source_ip=sentinel.source_ip,
            input_session_id=sentinel.input_session_id,
            input_content=sentinel.input_content,
            input_properties=sentinel.input_properties,
            input_references=sentinel.input_references,
            session_id=sentinel.session_id,
            content=sentinel.content,
            properties=sentinel.properties,
            references=sentinel.references,
        )
        create_call = call.create_log_entry(
            sentinel.service,
            sentinel.log_entry_type,
            source_ip=sentinel.source_ip,
            session_id=sentinel.input_session_id,
            content=sentinel.input_content,
            properties=sentinel.input_properties,
            references=sentinel.input_references,
        )
        close_call = call.close_log_entry(
            sentinel.log_entry_id,
            sentinel.result,
            session_id=sentinel.session_id,
            content=sentinel.content,
            properties=sentinel.properties,
            references=sentinel.references,
        )
        self.assertEqual(log_entry.get_calls(), [create_call, close_call])


class TestLoggerClientTest(TestCase):
    def test_create_log_entry_valid_args(self):
        calls: Iterable[tuple[Iterable, dict]] = (
            # args, kwargs
            ((sentinel.service, sentinel.log_entry_type), {}),
            ((sentinel.service,), {"log_entry_type": sentinel.log_entry_type}),
            ((), {"service": sentinel.service, "log_entry_type": sentinel.log_entry_type}),
            ((sentinel.service, sentinel.log_entry_type), {"session_id": sentinel.session_id}),
        )
        for args, kwargs in calls:
            with self.subTest(args=args, kwargs=kwargs):
                client = TestLoggerClient()
                entry_id = client.create_log_entry(*args, **kwargs)

                self.assertEqual(entry_id, sentinel.log_entry_id)
                self.assertEqual(client.mock.mock_calls, [call.create_log_entry(*args, **kwargs)])

    def test_create_log_entry_invalid_args(self):
        client = TestLoggerClient()
        calls = (
            partial(client.create_log_entry),
            partial(client.create_log_entry, sentinel.service),
            partial(client.create_log_entry, service=sentinel.service),
            partial(client.create_log_entry, sentinel.service, sentinel.log_entry_type, sentinel.unknown),
            partial(client.create_log_entry, sentinel.service, sentinel.log_entry_type, unknown=sentinel.unknown),
        )
        for kall in calls:
            with self.subTest(kall):
                with self.assertRaises(TypeError):
                    kall()
                self.assertEqual(client.mock.mock_calls, [])

    def test_close_log_entry_valid_args(self):
        calls: Iterable[tuple[Iterable, dict]] = (
            # args, kwargs
            ((sentinel.log_entry_id, sentinel.result), {}),
            ((sentinel.log_entry_id,), {"result": sentinel.result}),
            ((), {"log_entry_id": sentinel.log_entry_id, "result": sentinel.result}),
            ((sentinel.log_entry_id, sentinel.result), {"session_id": sentinel.session_id}),
        )
        for args, kwargs in calls:
            with self.subTest(args=args, kwargs=kwargs):
                client = TestLoggerClient()
                client.close_log_entry(*args, **kwargs)

                self.assertEqual(client.mock.mock_calls, [call.close_log_entry(*args, **kwargs)])

    def test_close_log_entry_invalid_args(self):
        client = TestLoggerClient()
        calls = (
            partial(client.close_log_entry),
            partial(client.close_log_entry, sentinel.log_entry_id),
            partial(client.close_log_entry, log_entry_id=sentinel.log_entry_id),
            partial(client.close_log_entry, sentinel.log_entry_id, sentinel.result, sentinel.unknown),
            partial(client.close_log_entry, sentinel.log_entry_id, sentinel.result, unknown=sentinel.unknown),
        )
        for kall in calls:
            with self.subTest(kall):
                with self.assertRaises(TypeError):
                    kall()
                self.assertEqual(client.mock.mock_calls, [])

    def test_create_session_valid_args(self):
        calls: Iterable[tuple[Iterable, dict]] = (
            # args, kwargs
            ((sentinel.user_id, sentinel.username), {}),
            ((sentinel.user_id,), {"username": sentinel.username}),
            ((), {"user_id": sentinel.user_id, "username": sentinel.username}),
        )
        for args, kwargs in calls:
            with self.subTest(args=args, kwargs=kwargs):
                client = TestLoggerClient()
                session_id = client.create_session(*args, **kwargs)

                self.assertEqual(session_id, sentinel.session_id)
                self.assertEqual(client.mock.mock_calls, [call.create_session(*args, **kwargs)])

    def test_create_session_invalid_args(self):
        client = TestLoggerClient()
        calls = (
            partial(client.create_session),
            partial(client.create_session, sentinel.user_id),
            partial(client.create_session, user_id=sentinel.user_id),
            partial(client.create_session, sentinel.user_id, sentinel.username, sentinel.unknown),
            partial(client.create_session, sentinel.user_id, sentinel.username, unknown=sentinel.unknown),
        )
        for kall in calls:
            with self.subTest(kall):
                with self.assertRaises(TypeError):
                    kall()
                self.assertEqual(client.mock.mock_calls, [])

    def test_close_session_valid_args(self):
        calls: Iterable[tuple[Iterable, dict]] = (
            # args, kwargs
            ((sentinel.session_id,), {}),
            ((), {"session_id": sentinel.session_id}),
        )
        for args, kwargs in calls:
            with self.subTest(args=args, kwargs=kwargs):
                client = TestLoggerClient()
                client.close_session(*args, **kwargs)

                self.assertEqual(client.mock.mock_calls, [call.close_session(*args, **kwargs)])

    def test_close_session_invalid_args(self):
        client = TestLoggerClient()
        calls = (
            partial(client.close_session),
            partial(client.close_session, sentinel.session_id, sentinel.unknown),
            partial(client.close_session, sentinel.session_id, unknown=sentinel.unknown),
        )
        for kall in calls:
            with self.subTest(kall):
                with self.assertRaises(TypeError):
                    kall()
                self.assertEqual(client.mock.mock_calls, [])

    def test_get_log_entry_types_valid_args(self):
        calls: Iterable[tuple[Iterable, dict]] = (
            # args, kwargs
            ((sentinel.service,), {}),
            ((), {"service": sentinel.service}),
        )
        for args, kwargs in calls:
            with self.subTest(args=args, kwargs=kwargs):
                client = TestLoggerClient()
                result = client.get_log_entry_types(*args, **kwargs)

                self.assertEqual(result, [])
                self.assertEqual(client.mock.mock_calls, [call.get_log_entry_types(*args, **kwargs)])

    def test_get_log_entry_types_invalid_args(self):
        client = TestLoggerClient()
        calls = (
            partial(client.get_log_entry_types),
            partial(client.get_log_entry_types, sentinel.service, sentinel.unknown),
            partial(client.get_log_entry_types, sentinel.service, unknown=sentinel.unknown),
        )
        for kall in calls:
            with self.subTest(kall):
                with self.assertRaises(TypeError):
                    kall()
                self.assertEqual(client.mock.mock_calls, [])

    def test_get_services_valid_args(self):
        client = TestLoggerClient()
        result = client.get_services()

        self.assertEqual(result, [])
        self.assertEqual(client.mock.mock_calls, [call.get_services()])

    def test_get_services_invalid_args(self):
        client = TestLoggerClient()
        calls = (
            partial(client.get_services, sentinel.unknown),
            partial(client.get_services, unknown=sentinel.unknown),
        )
        for kall in calls:
            with self.subTest(kall):
                with self.assertRaises(TypeError):
                    kall()
                self.assertEqual(client.mock.mock_calls, [])

    def test_get_results_valid_args(self):
        calls: Iterable[tuple[Iterable, dict]] = (
            # args, kwargs
            ((sentinel.service,), {}),
            ((), {"service": sentinel.service}),
        )
        for args, kwargs in calls:
            with self.subTest(args=args, kwargs=kwargs):
                client = TestLoggerClient()
                result = client.get_results(*args, **kwargs)

                self.assertEqual(result, {})
                self.assertEqual(client.mock.mock_calls, [call.get_results(*args, **kwargs)])

    def test_get_results_invalid_args(self):
        client = TestLoggerClient()
        calls = (
            partial(client.get_results),
            partial(client.get_results, sentinel.service, sentinel.unknown),
            partial(client.get_results, sentinel.service, unknown=sentinel.unknown),
        )
        for kall in calls:
            with self.subTest(kall):
                with self.assertRaises(TypeError):
                    kall()
                self.assertEqual(client.mock.mock_calls, [])

    def test_get_object_reference_types_valid_args(self):
        client = TestLoggerClient()
        result = client.get_object_reference_types()

        self.assertEqual(result, [])
        self.assertEqual(client.mock.mock_calls, [call.get_object_reference_types()])

    def test_get_object_reference_types_invalid_args(self):
        client = TestLoggerClient()
        calls = (
            partial(client.get_object_reference_types, sentinel.unknown),
            partial(client.get_object_reference_types, unknown=sentinel.unknown),
        )
        for kall in calls:
            with self.subTest(kall):
                with self.assertRaises(TypeError):
                    kall()
                self.assertEqual(client.mock.mock_calls, [])

    def test_register_service_valid_args(self):
        calls: Iterable[tuple[Iterable, dict]] = (
            # args, kwargs
            ((sentinel.service, sentinel.handle), {}),
            ((sentinel.service,), {"handle": sentinel.handle}),
            ((), {"service": sentinel.service, "handle": sentinel.handle}),
        )
        for args, kwargs in calls:
            with self.subTest(args=args, kwargs=kwargs):
                client = TestLoggerClient()
                client.register_service(*args, **kwargs)

                self.assertEqual(client.mock.mock_calls, [call.register_service(*args, **kwargs)])

    def test_register_service_invalid_args(self):
        client = TestLoggerClient()
        calls = (
            partial(client.register_service),
            partial(client.register_service, sentinel.service),
            partial(client.register_service, service=sentinel.service),
            partial(client.register_service, sentinel.service, sentinel.handle, sentinel.unknown),
            partial(client.register_service, sentinel.service, sentinel.handle, unknown=sentinel.unknown),
        )
        for kall in calls:
            with self.subTest(kall):
                with self.assertRaises(TypeError):
                    kall()
                self.assertEqual(client.mock.mock_calls, [])

    def test_register_results_valid_args(self):
        calls: Iterable[tuple[Iterable, dict]] = (
            # args, kwargs
            ((sentinel.service, sentinel.results), {}),
            ((sentinel.service,), {"results": sentinel.results}),
            ((), {"service": sentinel.service, "results": sentinel.results}),
        )
        for args, kwargs in calls:
            with self.subTest(args=args, kwargs=kwargs):
                client = TestLoggerClient()
                client.register_results(*args, **kwargs)

                self.assertEqual(client.mock.mock_calls, [call.register_results(*args, **kwargs)])

    def test_register_results_invalid_args(self):
        client = TestLoggerClient()
        calls = (
            partial(client.register_results),
            partial(client.register_results, sentinel.service),
            partial(client.register_results, service=sentinel.service),
            partial(client.register_results, sentinel.service, sentinel.results, sentinel.unknown),
            partial(client.register_results, sentinel.service, sentinel.results, unknown=sentinel.unknown),
        )
        for kall in calls:
            with self.subTest(kall):
                with self.assertRaises(TypeError):
                    kall()
                self.assertEqual(client.mock.mock_calls, [])

    def test_register_log_entry_types_valid_args(self):
        calls: Iterable[tuple[Iterable, dict]] = (
            # args, kwargs
            ((sentinel.service, sentinel.log_entry_types), {}),
            ((sentinel.service,), {"log_entry_types": sentinel.log_entry_types}),
            ((), {"service": sentinel.service, "log_entry_types": sentinel.log_entry_types}),
        )
        for args, kwargs in calls:
            with self.subTest(args=args, kwargs=kwargs):
                client = TestLoggerClient()
                client.register_log_entry_types(*args, **kwargs)

                self.assertEqual(client.mock.mock_calls, [call.register_log_entry_types(*args, **kwargs)])

    def test_register_log_entry_types_invalid_args(self):
        client = TestLoggerClient()
        calls = (
            partial(client.register_log_entry_types),
            partial(client.register_log_entry_types, sentinel.service),
            partial(client.register_log_entry_types, service=sentinel.service),
            partial(client.register_log_entry_types, sentinel.service, sentinel.log_entry_types, sentinel.unknown),
            partial(
                client.register_log_entry_types, sentinel.service, sentinel.log_entry_types, unknown=sentinel.unknown
            ),
        )
        for kall in calls:
            with self.subTest(kall):
                with self.assertRaises(TypeError):
                    kall()
                self.assertEqual(client.mock.mock_calls, [])

    def test_register_object_reference_types_valid_args(self):
        calls: Iterable[tuple[Iterable, dict]] = (
            # args, kwargs
            ((sentinel.reference_types,), {}),
            ((), {"reference_types": sentinel.reference_types}),
        )
        for args, kwargs in calls:
            with self.subTest(args=args, kwargs=kwargs):
                client = TestLoggerClient()
                client.register_object_reference_types(*args, **kwargs)

                self.assertEqual(client.mock.mock_calls, [call.register_object_reference_types(*args, **kwargs)])

    def test_register_object_reference_types_invalid_args(self):
        client = TestLoggerClient()
        calls = (
            partial(client.register_object_reference_types),
            partial(client.register_object_reference_types, sentinel.reference_types, sentinel.unknown),
            partial(client.register_object_reference_types, sentinel.reference_types, unknown=sentinel.unknown),
        )
        for kall in calls:
            with self.subTest(kall):
                with self.assertRaises(TypeError):
                    kall()
                self.assertEqual(client.mock.mock_calls, [])
