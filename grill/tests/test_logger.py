"""Tests for a logger module."""

from unittest import TestCase
from unittest.mock import Mock, call, sentinel

from grill.clients import LoggerClient
from grill.logger import Logger


class TestLogger(TestCase):
    """Test Logger class."""

    def setUp(self):
        self.client = Mock(spec=LoggerClient)

    def test_create_minimal(self):
        # Test new log entry with minimal arguments
        logger = Logger(sentinel.client, sentinel.service, sentinel.default_result)

        log_entry = logger.create(sentinel.log_entry_type)

        self.assertEqual(log_entry.logger, logger)
        self.assertEqual(log_entry.log_entry_type, sentinel.log_entry_type)
        self.assertIsNone(log_entry.source_ip)
        self.assertIsNone(log_entry.session_id)
        self.assertIsNone(log_entry.input_content)
        self.assertIsNone(log_entry.input_properties)
        self.assertIsNone(log_entry.input_references)
        self.assertIsNone(log_entry.content)
        self.assertEqual(log_entry.properties, {})
        self.assertEqual(log_entry.references, {})
        self.assertIsNone(log_entry.result)
        self.assertIsNone(log_entry.entry_id)

    def test_create(self):
        logger = Logger(sentinel.client, sentinel.service, sentinel.default_result)

        log_entry = logger.create(
            sentinel.log_entry_type,
            source_ip=sentinel.source_ip,
            session_id=sentinel.session_id,
            content=sentinel.content,
            properties=sentinel.properties,
            references=sentinel.references,
        )

        self.assertEqual(log_entry.logger, logger)
        self.assertEqual(log_entry.log_entry_type, sentinel.log_entry_type)
        self.assertEqual(log_entry.source_ip, sentinel.source_ip)
        self.assertEqual(log_entry.session_id, sentinel.session_id)
        self.assertEqual(log_entry.input_content, sentinel.content)
        self.assertEqual(log_entry.input_properties, sentinel.properties)
        self.assertEqual(log_entry.input_references, sentinel.references)
        self.assertIsNone(log_entry.content)
        self.assertEqual(log_entry.properties, {})
        self.assertEqual(log_entry.references, {})
        self.assertIsNone(log_entry.result)
        self.assertIsNone(log_entry.entry_id)

    def test_create_session(self):
        self.client.create_session.return_value = sentinel.session_id
        logger = Logger(self.client, sentinel.service, sentinel.default_result)

        session_id = logger.create_session(sentinel.user_id, sentinel.username)

        self.assertEqual(session_id, sentinel.session_id)
        self.assertEqual(self.client.mock_calls, [call.create_session(sentinel.user_id, sentinel.username)])

    def test_close_session(self):
        logger = Logger(self.client, sentinel.service, sentinel.default_result)

        logger.close_session(sentinel.session_id)

        self.assertEqual(self.client.mock_calls, [call.close_session(sentinel.session_id)])


class TestLogEntry(TestCase):
    """Test LogEntry class."""

    def setUp(self):
        self.client = Mock(spec=LoggerClient)

    def test_open_minimal(self):
        # Test opening log entry with minimal arguments
        self.client.create_log_entry.return_value = sentinel.entry_id

        logger = Logger(self.client, sentinel.service, sentinel.default_result)
        log_entry = logger.create(sentinel.log_entry_type)

        log_entry.open()

        self.assertEqual(log_entry.entry_id, sentinel.entry_id)
        calls = [
            call.create_log_entry(
                sentinel.service,
                sentinel.log_entry_type,
                source_ip=None,
                session_id=None,
                content=None,
                properties=None,
                references=None,
            )
        ]
        self.assertEqual(self.client.mock_calls, calls)

    def test_open(self):
        self.client.create_log_entry.return_value = sentinel.entry_id

        logger = Logger(self.client, sentinel.service, sentinel.default_result)
        log_entry = logger.create(
            sentinel.log_entry_type,
            source_ip=sentinel.source_ip,
            session_id=sentinel.session_id,
            content=sentinel.content,
            properties=sentinel.properties,
            references=sentinel.references,
        )

        log_entry.open()

        self.assertEqual(log_entry.entry_id, sentinel.entry_id)
        calls = [
            call.create_log_entry(
                sentinel.service,
                sentinel.log_entry_type,
                source_ip=sentinel.source_ip,
                session_id=sentinel.session_id,
                content=sentinel.content,
                properties=sentinel.properties,
                references=sentinel.references,
            )
        ]
        self.assertEqual(self.client.mock_calls, calls)

    def test_close_minimal(self):
        # Test closing log entry with minimal arguments
        logger = Logger(self.client, sentinel.service, sentinel.default_result)
        log_entry = logger.create(sentinel.log_entry_type)
        log_entry.entry_id = sentinel.entry_id  # Simulate open request

        log_entry.close()

        calls = [
            call.close_log_entry(
                sentinel.entry_id, sentinel.default_result, session_id=None, content=None, properties={}, references={}
            )
        ]
        self.assertEqual(self.client.mock_calls, calls)

    def test_close(self):
        logger = Logger(self.client, sentinel.service, sentinel.default_result)
        log_entry = logger.create(sentinel.log_entry_type)
        log_entry.entry_id = sentinel.entry_id  # Simulate open request
        log_entry.session_id = sentinel.session_id
        log_entry.result = sentinel.result
        log_entry.content = sentinel.content
        log_entry.properties = sentinel.properties
        log_entry.references = sentinel.references

        log_entry.close()

        calls = [
            call.close_log_entry(
                sentinel.entry_id,
                sentinel.result,
                session_id=sentinel.session_id,
                content=sentinel.content,
                properties=sentinel.properties,
                references=sentinel.references,
            )
        ]
        self.assertEqual(self.client.mock_calls, calls)

    def test_context_manager(self):
        self.client.create_log_entry.return_value = sentinel.entry_id

        logger = Logger(self.client, sentinel.service, sentinel.default_result)
        with logger.create(sentinel.log_entry_type) as log_entry:
            log_entry.result = sentinel.result

        calls = [
            call.create_log_entry(
                sentinel.service,
                sentinel.log_entry_type,
                source_ip=None,
                session_id=None,
                content=None,
                properties=None,
                references=None,
            ),
            call.close_log_entry(
                sentinel.entry_id, sentinel.result, session_id=None, content=None, properties={}, references={}
            ),
        ]
        self.assertEqual(self.client.mock_calls, calls)

    def test_context_manager_error(self):
        self.client.create_log_entry.return_value = sentinel.entry_id

        logger = Logger(self.client, sentinel.service, sentinel.default_result)
        with self.assertRaisesRegex(Exception, "FourtyThree"):
            with logger.create(sentinel.log_entry_type):
                raise Exception("FourtyThree")

        calls = [
            call.create_log_entry(
                sentinel.service,
                sentinel.log_entry_type,
                source_ip=None,
                session_id=None,
                content=None,
                properties=None,
                references=None,
            ),
            call.close_log_entry(
                sentinel.entry_id, sentinel.default_result, session_id=None, content=None, properties={}, references={}
            ),
        ]
        self.assertEqual(self.client.mock_calls, calls)
