"""Tests for grpc module."""

from datetime import datetime, timezone
from enum import Enum
from typing import Any, cast
from unittest import TestCase
from unittest.mock import call, sentinel

from fred_api.logger.log_entry_common_types_pb2 import LogEntryDoesNotExist as GrpcLogEntryDoesNotExist
from fred_api.logger.service_logger_grpc_pb2 import (
    CloseLogEntryReply,
    CloseLogEntryRequest,
    CloseSessionReply,
    CloseSessionRequest,
    CreateLogEntryReply,
    CreateLogEntryRequest,
    CreateSessionReply,
    CreateSessionRequest,
    GetLogEntryTypesReply,
    GetLogEntryTypesRequest,
    GetObjectReferenceTypesReply,
    GetResultsReply,
    GetResultsRequest,
    GetServicesReply,
    RegisterLogEntryTypesRequest,
    RegisterObjectReferenceTypesRequest,
    RegisterResultsRequest,
    RegisterServiceRequest,
)
from fred_api.logger.service_search_logger_history_grpc_pb2 import (
    GetLogEntryInfoReply,
    GetLogEntryInfoRequest,
    ListLogEntriesReply,
    ListLogEntriesRequest,
)
from frgal.utils import AsyncTestClientMixin, TestClientMixin, make_awaitable
from google.protobuf.empty_pb2 import Empty as _google_protobuf_Empty
from grpc import RpcError, StatusCode
from grpc._channel import _MultiThreadedRendezvous as _Rendezvous, _RPCState

from grill.clients import (
    DummyLoggerClient,
    ListProperty,
    LoggerClient,
    LogProperty,
    SearchLoggerHistoryAsyncClient,
    get_logger_client,
    wrap_rpc_errors,
)
from grill.exceptions import InvalidArgument, LogEntryDoesNotExist, SessionDoesNotExist

try:
    from unittest import IsolatedAsyncioTestCase
except ImportError:  # pragma: no cover
    from asynctest import TestCase as IsolatedAsyncioTestCase  # type: ignore


class EmptyEnum(str, Enum):
    """Emtpy enum."""


class GetLoggerClientTest(TestCase):
    def test_class_only(self):
        obj = get_logger_client("grill.clients.DummyLoggerClient")

        self.assertIsInstance(obj, DummyLoggerClient)

    def test_class_kwargs(self):
        obj = get_logger_client("grill.tests.test_clients.TestLoggerClient", netloc=sentinel.netloc)

        self.assertIsInstance(obj, LoggerClient)
        self.assertEqual(cast(TestLoggerClient, obj).netloc, sentinel.netloc)

    def test_invalid_path(self):
        data = (
            # path, error_message
            ("", "is not a valid dotted path"),
            (".", "Empty module name"),
            (".invalid", "Empty module name"),
            ("invalid", "is not a valid dotted path"),
            ("invalid.", "No module named"),
            ("invalid.path", "No module named"),
            ("invalid..path", "No module named"),
            ("grill.tests.invalid.TestCase", "No module named"),
            ("grill.tests.test_clients.", "does not have a class or attribute"),
            ("grill.tests.test_clients.InvalidTestCase", "does not have a class or attribute"),
        )
        for path, error in data:
            with self.subTest(path=path):
                with self.assertRaisesRegex(ImportError, error):
                    get_logger_client(path)

    def test_invalid_path_not_callable(self):
        with self.assertRaisesRegex(TypeError, "object is not callable"):
            get_logger_client("grill.tests.test_clients")

    def test_invalid_kwargs(self):
        data = (
            {"invalid": sentinel.invalid},
            {"netloc": sentinel.netloc, "invalid": sentinel.invalid},
        )
        for kwargs in data:
            with self.subTest(kwargs=kwargs):
                with self.assertRaises(TypeError):
                    get_logger_client("grill.tests.test_clients.TestLoggerClient", **kwargs)


class WrapRpcErrorsTest(TestCase):
    def test_arguments(self):
        @wrap_rpc_errors
        def _test(*args, **kwargs):
            self.assertEqual(args, (sentinel.arg1, sentinel.arg2))
            self.assertEqual(kwargs, {"key1": sentinel.value1, "key2": sentinel.value2})

        _test(sentinel.arg1, sentinel.arg2, key1=sentinel.value1, key2=sentinel.value2)

    def test_result(self):
        @wrap_rpc_errors
        def _test():
            return sentinel.result

        self.assertEqual(_test(), sentinel.result)

    def test_invalid_argument(self):
        @wrap_rpc_errors
        def _test():
            state = _RPCState((), "", "", StatusCode.INVALID_ARGUMENT, "Unknown error")
            raise _Rendezvous(state, None, None, None)

        with self.assertRaisesRegex(InvalidArgument, "Unknown error"):
            _test()

    def test_unknown_error(self):
        @wrap_rpc_errors
        def _test():
            state = _RPCState((), "", "", StatusCode.UNKNOWN, "Unknown error")
            raise _Rendezvous(state, None, None, None)

        with self.assertRaisesRegex(RpcError, "Unknown error"):
            _test()


class DummyLoggerClientTest(TestCase):
    def setUp(self):
        self.client = DummyLoggerClient()

    def test_create_log_entry(self):
        entry_id = self.client.create_log_entry("Universe", "Answer")
        self.assertEqual(entry_id, "DUMMY_LOG_ENTRY_ID")

    def test_close_log_entry(self):
        # Mypy complains about checking the return value of function which returns nothing.
        self.assertIsNone(self.client.close_log_entry("FourtyTwo", "Success"))  # type: ignore[func-returns-value]

    def test_create_session(self):
        session_id = self.client.create_session(4242, "Arthur Dent")
        self.assertEqual(session_id, "DUMMY_SESSION_ID")

    def test_close_session(self):
        # Mypy complains about checking the return value of function which returns nothing.
        self.assertIsNone(self.client.close_session("Life"))  # type: ignore[func-returns-value]

    def test_get_log_entry_types(self):
        self.assertEqual(self.client.get_log_entry_types(service="Earth"), [])

    def test_get_services(self):
        self.assertEqual(self.client.get_services(), [])

    def test_get_results(self):
        self.assertEqual(self.client.get_results(service="Earth"), {})

    def test_get_object_reference_types(self):
        self.assertEqual(self.client.get_object_reference_types(), [])

    def test_register_service(self):
        # Mypy complains about checking the return value of function which returns nothing.
        self.assertIsNone(self.client.register_service("Earth", "earth"))  # type: ignore[func-returns-value]

    def test_register_results(self):
        # Mypy complains about checking the return value of function which returns nothing.
        self.assertIsNone(self.client.register_results("Earth", {"answered": 42}))  # type: ignore[func-returns-value]

    def test_register_results_simple(self):
        # Test registration of results as an iterable of names only.
        self.assertIsNone(self.client.register_results("Earth", ("answered",)))  # type: ignore[func-returns-value]

    def test_register_log_entry_types(self):
        # Mypy complains about checking the return value of function which returns nothing.
        self.assertIsNone(self.client.register_log_entry_types("Earth", ["Ask"]))  # type: ignore[func-returns-value]

    def test_register_object_reference_types(self):
        # Mypy complains about checking the return value of function which returns nothing.
        self.assertIsNone(self.client.register_object_reference_types(["Human"]))  # type: ignore[func-returns-value]


class TestLoggerClient(TestClientMixin, LoggerClient):
    """Testing version of a LoggerClient."""


class LoggerClientTest(TestCase):
    def setUp(self):
        self.client = TestLoggerClient(sentinel.netloc)

    def _test_create_log_entry(self, request: CreateLogEntryRequest, **kwargs: Any) -> None:
        reply = CreateLogEntryReply()
        reply.data.log_entry_id.value = "FourtyTwo"
        self.client.mock.return_value = reply

        entry_id = self.client.create_log_entry("Universe", "Answer", **kwargs)

        self.assertEqual(entry_id, "FourtyTwo")
        # Finish and check request
        request.service.value = "Universe"
        request.log_entry_type.value = "Answer"
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Logger.Api.Logger/create_log_entry", timeout=None)],
        )

    def test_create_log_entry_minimal(self):
        # Test create_log_entry with minimum arguments
        request = CreateLogEntryRequest()
        self._test_create_log_entry(request)

    def test_create_log_entry_source_ip(self):
        request = CreateLogEntryRequest()
        request.source_ip.value = "256.0.0.1"
        self._test_create_log_entry(request, source_ip="256.0.0.1")

    def test_create_log_entry_session_id(self):
        request = CreateLogEntryRequest()
        request.session_id.value = "Life"
        self._test_create_log_entry(request, session_id="Life")

    def test_create_log_entry_content(self):
        request = CreateLogEntryRequest()
        request.content.value = "TheQuestion"
        self._test_create_log_entry(request, content="TheQuestion")

    def test_create_log_entry_properties_simple(self):
        request = CreateLogEntryRequest()
        request.properties["quality"].values.add(value="absolute")
        self._test_create_log_entry(request, properties={"quality": LogProperty("absolute", None)})

    def test_create_log_entry_properties_children(self):
        request = CreateLogEntryRequest()
        request.properties["quality"].values.add(value="absolute", children={"control": "yes"})
        self._test_create_log_entry(request, properties={"quality": LogProperty("absolute", {"control": "yes"})})

    def test_create_log_entry_properties_list(self):
        request = CreateLogEntryRequest()
        request.properties["quality"].values.add(value="absolute")
        request.properties["quality"].values.add(value="relative")
        self._test_create_log_entry(
            request, properties={"quality": [LogProperty("absolute", None), LogProperty("relative", None)]}
        )

    def test_create_log_entry_properties_dict(self):
        # Test properties passed as a simple dictionary.
        request = CreateLogEntryRequest()
        request.properties["quality"].values.add(value="absolute")
        self._test_create_log_entry(request, properties={"quality": "absolute"})

    def test_create_log_entry_properties_dict_items(self):
        # Test properties passed as a simple dictionary with list values.
        request = CreateLogEntryRequest()
        request.properties["quality"].values.add(value="absolute")
        request.properties["quality"].values.add(value="relative")
        self._test_create_log_entry(request, properties={"quality": ["absolute", "relative"]})

    def test_create_log_entry_references_simple(self):
        request = CreateLogEntryRequest()
        request.references["footnote"].references.add(value="196")
        self._test_create_log_entry(request, references={"footnote": "196"})

    def test_create_log_entry_references_list(self):
        request = CreateLogEntryRequest()
        request.references["footnote"].references.add(value="196")
        request.references["footnote"].references.add(value="197")
        self._test_create_log_entry(request, references={"footnote": ["196", "197"]})

    def _test_close_log_entry(self, request: CloseLogEntryRequest, **kwargs: Any) -> None:
        self.client.mock.return_value = CloseLogEntryReply()

        self.client.close_log_entry("FourtyTwo", "Success", **kwargs)

        # Finish and check request
        request.log_entry_id.value = "FourtyTwo"
        request.result_name = "Success"
        self.assertEqual(
            self.client.mock.mock_calls, [call(request, method="/Fred.Logger.Api.Logger/close_log_entry", timeout=None)]
        )

    def test_close_log_entry_minimal(self):
        # Test close_log_entry with minimum arguments
        request = CloseLogEntryRequest()
        self._test_close_log_entry(request)

    def test_close_log_entry_session_id(self):
        request = CloseLogEntryRequest()
        request.session_id.value = "Life"
        self._test_close_log_entry(request, session_id="Life")

    def test_close_log_entry_content(self):
        request = CloseLogEntryRequest()
        request.content.value = "TheQuestion"
        self._test_close_log_entry(request, content="TheQuestion")

    def test_close_log_entry_properties_simple(self):
        request = CloseLogEntryRequest()
        request.properties["quality"].values.add(value="absolute")
        self._test_close_log_entry(request, properties={"quality": LogProperty("absolute", None)})

    def test_close_log_entry_properties_children(self):
        request = CloseLogEntryRequest()
        request.properties["quality"].values.add(value="absolute", children={"control": "yes"})
        self._test_close_log_entry(request, properties={"quality": LogProperty("absolute", {"control": "yes"})})

    def test_close_log_entry_properties_list(self):
        request = CloseLogEntryRequest()
        request.properties["quality"].values.add(value="absolute")
        request.properties["quality"].values.add(value="relative")
        self._test_close_log_entry(
            request, properties={"quality": [LogProperty("absolute", None), LogProperty("relative", None)]}
        )

    def test_close_log_entry_properties_dict(self):
        # Test properties passed as a simple dictionary.
        request = CloseLogEntryRequest()
        request.properties["quality"].values.add(value="absolute")
        self._test_close_log_entry(request, properties={"quality": "absolute"})

    def test_close_log_entry_properties_dict_items(self):
        # Test properties passed as a simple dictionary with list values.
        request = CloseLogEntryRequest()
        request.properties["quality"].values.add(value="absolute")
        request.properties["quality"].values.add(value="relative")
        self._test_close_log_entry(request, properties={"quality": ["absolute", "relative"]})

    def test_close_log_entry_references_simple(self):
        request = CloseLogEntryRequest()
        request.references["footnote"].references.add(value="196")
        self._test_close_log_entry(request, references={"footnote": "196"})

    def test_close_log_entry_references_list(self):
        request = CloseLogEntryRequest()
        request.references["footnote"].references.add(value="196")
        request.references["footnote"].references.add(value="197")
        self._test_close_log_entry(request, references={"footnote": ["196", "197"]})

    def test_close_log_entry_error(self):
        reply = CloseLogEntryReply()
        reply.exception.log_entry_does_not_exist.CopyFrom(GrpcLogEntryDoesNotExist())
        self.client.mock.return_value = reply

        with self.assertRaises(LogEntryDoesNotExist):
            self.client.close_log_entry("FourtyTwo", "Success")

        # Check call
        request = CloseLogEntryRequest()
        request.log_entry_id.value = "FourtyTwo"
        request.result_name = "Success"
        self.assertEqual(
            self.client.mock.mock_calls, [call(request, method="/Fred.Logger.Api.Logger/close_log_entry", timeout=None)]
        )

    def test_create_session(self):
        reply = CreateSessionReply()
        reply.data.session_id.value = "Life"
        self.client.mock.return_value = reply

        self.client.create_session(4242, "Arthur Dent")

        # Finish and check request
        request = CreateSessionRequest()
        request.user_id.value = 4242
        request.username.value = "Arthur Dent"
        self.assertEqual(
            self.client.mock.mock_calls, [call(request, method="/Fred.Logger.Api.Logger/create_session", timeout=None)]
        )

    def test_close_session(self):
        self.client.mock.return_value = CloseSessionReply()

        self.client.close_session("Life")

        # Finish and check request
        request = CloseSessionRequest()
        request.session_id.value = "Life"
        self.assertEqual(
            self.client.mock.mock_calls, [call(request, method="/Fred.Logger.Api.Logger/close_session", timeout=None)]
        )

    def test_close_session_error(self):
        reply = CloseSessionReply()
        reply.exception.session_does_not_exist.CopyFrom(CloseSessionReply.Exception.SessionDoesNotExist())
        self.client.mock.return_value = reply

        with self.assertRaises(SessionDoesNotExist):
            self.client.close_session("Life")

        # Check call
        request = CloseSessionRequest()
        request.session_id.value = "Life"
        self.assertEqual(
            self.client.mock.mock_calls, [call(request, method="/Fred.Logger.Api.Logger/close_session", timeout=None)]
        )

    def _test_get_log_entry_types(self, result: list[str]) -> None:
        self.assertEqual(self.client.get_log_entry_types(service="Earth"), result)

        # Check call
        request = GetLogEntryTypesRequest()
        request.service.value = "Earth"
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Logger.Api.Logger/get_log_entry_types", timeout=None)],
        )

    def test_get_log_entry_types_empty(self):
        self.client.mock.return_value = GetLogEntryTypesReply()

        self._test_get_log_entry_types([])

    def test_get_log_entry_types(self):
        reply = GetLogEntryTypesReply()
        reply.data.types.add(value="hello")
        reply.data.types.add(value="bye")
        self.client.mock.return_value = reply

        self._test_get_log_entry_types(["hello", "bye"])

    def _test_get_services(self, result: list[str]) -> None:
        self.assertEqual(self.client.get_services(), result)

        # Check call
        request = _google_protobuf_Empty()
        self.assertEqual(
            self.client.mock.mock_calls, [call(request, method="/Fred.Logger.Api.Logger/get_services", timeout=None)]
        )

    def test_get_services_empty(self):
        self.client.mock.return_value = GetServicesReply()

        self._test_get_services([])

    def test_get_services(self):
        reply = GetServicesReply()
        reply.data.services.add(value="Alpha")
        reply.data.services.add(value="Beta")
        reply.data.services.add(value="Gama")
        self.client.mock.return_value = reply

        self._test_get_services(["Alpha", "Beta", "Gama"])

    def _test_get_results(self, result: dict[str, int]) -> None:
        self.assertEqual(self.client.get_results(service="Earth"), result)

        # Check call
        request = GetResultsRequest()
        request.service.value = "Earth"
        self.assertEqual(
            self.client.mock.mock_calls, [call(request, method="/Fred.Logger.Api.Logger/get_results", timeout=None)]
        )

    def test_get_results_empty(self):
        self.client.mock.return_value = GetResultsReply()

        self._test_get_results({})

    def test_get_results(self):
        reply = GetResultsReply()
        reply.data.result_code_map["ok"] = 0
        reply.data.result_code_map["error"] = 1
        reply.data.result_code_map["mega-error"] = 9999
        self.client.mock.return_value = reply

        self._test_get_results({"ok": 0, "error": 1, "mega-error": 9999})

    def _test_get_object_reference_types(self, result: list[str]) -> None:
        self.assertEqual(self.client.get_object_reference_types(), result)

        # Check call
        request = _google_protobuf_Empty()
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Logger.Api.Logger/get_object_reference_types", timeout=None)],
        )

    def test_get_object_reference_types_empty(self):
        self.client.mock.return_value = GetObjectReferenceTypesReply()

        self._test_get_object_reference_types([])

    def test_get_object_reference_types(self):
        reply = GetObjectReferenceTypesReply()
        reply.data.object_types.add(value="number")
        reply.data.object_types.add(value="boolean")
        reply.data.object_types.add(value="string")
        self.client.mock.return_value = reply

        self._test_get_object_reference_types(["number", "boolean", "string"])

    def test_register_service(self):
        self.client.mock.return_value = _google_protobuf_Empty()

        self.assertIsNone(self.client.register_service("Earth", "earth"))

        # Check call
        request = RegisterServiceRequest()
        request.service_name.value = "Earth"
        request.service_handle = "earth"
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Logger.Api.Logger/register_service", timeout=None)],
        )

    def test_register_results(self):
        self.client.mock.return_value = _google_protobuf_Empty()

        self.assertIsNone(self.client.register_results("Earth", {"answered": 42}))

        # Check call
        request = RegisterResultsRequest()
        request.service.value = "Earth"
        request.results.add(code=42, name="answered")
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Logger.Api.Logger/register_results", timeout=None)],
        )

    def test_register_results_simple(self):
        # Test registration of results as an iterable of names only.
        self.client.mock.side_effect = [GetResultsReply(), _google_protobuf_Empty()]

        self.assertIsNone(self.client.register_results("Earth", ("answered",)))

        # Check calls
        get_request = GetResultsRequest()
        get_request.service.value = "Earth"
        reg_request = RegisterResultsRequest()
        reg_request.service.value = "Earth"
        reg_request.results.add(code=0, name="answered")
        calls = [
            call(get_request, method="/Fred.Logger.Api.Logger/get_results", timeout=None),
            call(reg_request, method="/Fred.Logger.Api.Logger/register_results", timeout=None),
        ]
        self.assertEqual(self.client.mock.mock_calls, calls)

    def test_register_results_simple_known(self):
        # Test registration of results as an iterable of names only - results exists.
        reply = GetResultsReply()
        reply.data.result_code_map["answered"] = 42
        self.client.mock.side_effect = [reply, _google_protobuf_Empty()]

        self.assertIsNone(self.client.register_results("Earth", ("answered",)))

        # Check call
        get_request = GetResultsRequest()
        get_request.service.value = "Earth"
        calls = [call(get_request, method="/Fred.Logger.Api.Logger/get_results", timeout=None)]
        self.assertEqual(self.client.mock.mock_calls, calls)

    def test_register_results_simple_detect(self):
        # Test registration of results as an iterable of names only - test available ids are detected correctly.
        reply = GetResultsReply()
        reply.data.result_code_map["ok"] = 0
        reply.data.result_code_map["error1"] = 1
        reply.data.result_code_map["error2"] = 2
        reply.data.result_code_map["error4"] = 4
        self.client.mock.side_effect = [reply, _google_protobuf_Empty(), _google_protobuf_Empty()]

        self.assertIsNone(
            self.client.register_results("Earth", ("ok", "error1", "error2", "error3", "error4", "error5"))
        )

        # Check calls
        get_request = GetResultsRequest()
        get_request.service.value = "Earth"
        reg_request = RegisterResultsRequest()
        reg_request.service.value = "Earth"
        reg_request.results.add(code=5, name="error3")
        reg_request.results.add(code=6, name="error5")
        calls = [
            call(get_request, method="/Fred.Logger.Api.Logger/get_results", timeout=None),
            call(reg_request, method="/Fred.Logger.Api.Logger/register_results", timeout=None),
        ]
        self.assertEqual(self.client.mock.mock_calls, calls)

    def test_register_results_empty_dict(self):
        self.assertIsNone(self.client.register_results("Earth", {}))

        self.assertEqual(self.client.mock.mock_calls, [])

    def test_register_results_empty_enum(self):
        reply = GetResultsReply()
        self.client.mock.side_effect = [reply]

        self.assertIsNone(self.client.register_results("Earth", EmptyEnum))

        # Check calls
        get_request = GetResultsRequest()
        get_request.service.value = "Earth"
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(get_request, method="/Fred.Logger.Api.Logger/get_results", timeout=None)],
        )

    def test_register_log_entry_types(self):
        self.client.mock.return_value = _google_protobuf_Empty()

        self.assertIsNone(self.client.register_log_entry_types("Earth", ["Ask"]))

        # Check call
        request = RegisterLogEntryTypesRequest()
        request.service.value = "Earth"
        request.log_entry_types.add(value="Ask")
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Logger.Api.Logger/register_log_entry_types", timeout=None)],
        )

    def test_register_log_entry_types_empty_enum(self):
        self.assertIsNone(self.client.register_log_entry_types("Earth", EmptyEnum))

        self.assertEqual(self.client.mock.mock_calls, [])

    def test_register_object_reference_types(self):
        self.client.mock.return_value = _google_protobuf_Empty()

        self.assertIsNone(self.client.register_object_reference_types(["Human"]))

        # Check call
        request = RegisterObjectReferenceTypesRequest()
        request.object_types.add(value="Human")
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Logger.Api.Logger/register_object_reference_types", timeout=None)],
        )

    def test_register_object_reference_types_empty_enum(self):
        self.assertIsNone(self.client.register_object_reference_types(EmptyEnum))

        self.assertEqual(self.client.mock.mock_calls, [])


class TestSearchLoggerHistoryAsyncClient(AsyncTestClientMixin, SearchLoggerHistoryAsyncClient):
    """Testing version of a SearchLoggerHistoryAsyncClient."""


class SearchLoggerHistoryAsyncClientTest(IsolatedAsyncioTestCase):
    """Test SearchLoggerHistoryAsyncClient class."""

    def setUp(self):
        self.client = TestSearchLoggerHistoryAsyncClient(sentinel.netloc)

    async def test_list_log_entries_empty(self):
        reply = ListLogEntriesReply()
        self.client.mock.return_value = make_awaitable(reply)
        self.assertEqual(
            await self.client.list_log_entries(
                service="Globe",
                timestamp_from=datetime(1988, 11, 25, 18, 0),
                timestamp_to=datetime(1988, 12, 24, 18, 30),
                limit=100,
                log_entry_types=["RimmersDiary"],
                user="Albert",
                reference_type="dog",
                reference_value="42",
            ),
            [],
        )

        # Check call
        request = ListLogEntriesRequest()
        request.service.value = "Globe"
        request.timestamp_from.FromDatetime(datetime(1988, 11, 25, 18, 0))
        request.timestamp_to.FromDatetime(datetime(1988, 12, 24, 18, 30))
        request.count_limit = 100
        request.log_entry_types.add(value="RimmersDiary")
        request.username.value = "Albert"
        request.reference.type.value = "dog"
        request.reference.value.value = "42"

        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Logger.Api.SearchLoggerHistory/list_log_entries", timeout=None)],
        )

    async def _test_list_log_entries(self, filters: dict[str, Any], request: ListLogEntriesRequest) -> None:
        reply = ListLogEntriesReply()
        reply.data.log_entry_ids.add(value="GazpachoSoupDay")
        self.client.mock.return_value = make_awaitable(reply)

        self.assertEqual(
            await self.client.list_log_entries(
                service="Globe",
                timestamp_from=datetime(1988, 11, 25, 18, 0),
                timestamp_to=datetime(1988, 12, 24, 18, 30),
                limit=100,
                **filters,
            ),
            ["GazpachoSoupDay"],
        )

        # Check call
        request.service.value = "Globe"
        request.timestamp_from.FromDatetime(datetime(1988, 11, 25, 18, 0))
        request.timestamp_to.FromDatetime(datetime(1988, 12, 24, 18, 30))
        request.count_limit = 100

        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Logger.Api.SearchLoggerHistory/list_log_entries", timeout=None)],
        )

    async def test_list_log_entries(self):
        request = ListLogEntriesRequest()
        await self._test_list_log_entries({}, request)

    async def test_list_log_entries_username(self):
        request = ListLogEntriesRequest()
        request.username.value = "Albert"
        await self._test_list_log_entries({"user": "Albert"}, request)

    async def test_list_log_entries_user_id(self):
        request = ListLogEntriesRequest()
        request.user_id.value = 1001
        await self._test_list_log_entries({"user": 1001}, request)

    async def test_list_log_entries_log_entry_types(self):
        request = ListLogEntriesRequest()
        request.log_entry_types.add(value="RimmersDiary")
        await self._test_list_log_entries({"log_entry_types": ["RimmersDiary"]}, request)

    async def test_list_log_entries_reference(self):
        request = ListLogEntriesRequest()
        request.reference.type.value = "dog"
        request.reference.value.value = "42"
        await self._test_list_log_entries({"reference_type": "dog", "reference_value": "42"}, request)

    async def test_list_log_entries_proprerties(self):
        request = ListLogEntriesRequest()
        property = request.properties.add()
        property.name = "soup"
        property.value = "gazpacho"
        property = request.properties.add()
        property.name = "animal"
        property.value = "dog"
        await self._test_list_log_entries(
            {"properties": [ListProperty("soup", "gazpacho"), ListProperty("animal", "dog")]},
            request,
        )

    async def test_get_log_entry_info(self):
        reply = GetLogEntryInfoReply()
        reply.data.log_entry_info.time_begin.FromDatetime(datetime(1988, 11, 25, 18))
        self.client.mock.return_value = make_awaitable(reply)

        result = await self.client.get_log_entry_info("GazpachoSoupDay")

        self.assertEqual(result["time_begin"], datetime(1988, 11, 25, 18, tzinfo=timezone.utc))
        self.assertNotIn("properties", result)

        # Check call
        request = GetLogEntryInfoRequest()
        request.log_entry_id.value = "GazpachoSoupDay"

        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Logger.Api.SearchLoggerHistory/get_log_entry_info", timeout=None)],
        )
